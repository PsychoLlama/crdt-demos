// @flow
module.exports = {
  preset: '@freighter/scripts',
  transform: {
    '\\.js$': 'babel-jest',
  },
};

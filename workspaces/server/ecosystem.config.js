// @flow
module.exports = {
  apps: [
    {
      name: 'Relay Server',
      script: 'dist/index.js',
      instances: 1,
      autorestart: true,
      watch: ['dist'],
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
      },
    },
  ],
};

// @flow
import { createCounter } from '@crdt-demo/crdts';
import type Emitter from 'events';

export default function synchronizeCounter(socket: Emitter) {
  const counter = createCounter();
  const subscriptions = [];
  const emitChange = count => subscriptions.forEach(fn => fn(count));

  socket.on('hydrate', operations => {
    operations.forEach(counter.applyUpdate);
    emitChange(counter.getCount());
  });

  socket.on('updates', updates => {
    updates.forEach(counter.applyUpdate);
    emitChange(counter.getCount());
  });

  // Sync offline edits on reconnect. If the server doesn't get one
  // of your updates, you'll be out of sync with everyone else.
  socket.on('connect', () => {
    const operations = counter.serialize();
    socket.emit('updates', operations);
  });

  return {
    getCount: counter.getCount,

    add(count: number) {
      const update = counter.add(count);
      socket.emit('updates', [update]);
      emitChange(counter.getCount());
    },

    subscribe(fn: (count: number) => mixed) {
      subscriptions.push(fn);

      // Unsubscribe
      return () => {
        const handlerIndex = subscriptions.findIndex(handler => handler === fn);
        subscriptions.splice(handlerIndex, 1);
      };
    },
  };
}

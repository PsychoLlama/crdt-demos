// @flow
import { createCounter } from '@crdt-demo/crdts';
import Emitter from 'events';

import synchronizeCounter from '../synchronize-counter';

describe('synchronizeCounter(...)', () => {
  class Socket extends Emitter {}

  const createUpdate = (count: number) => {
    const counter = createCounter();

    return counter.add(count);
  };

  const createState = (values: number[]) => {
    const counter = createCounter();
    values.forEach(counter.add);

    return counter.serialize();
  };

  const setup = () => {
    const socket = new Socket();
    const counter = synchronizeCounter(socket);

    return {
      counter,
      socket,
    };
  };

  it('returns a counter interface', () => {
    const { counter } = setup();

    expect(counter).toMatchObject({
      subscribe: expect.any(Function),
      getCount: expect.any(Function),
      add: expect.any(Function),
    });
  });

  it('applies updates from the socket', () => {
    const { socket, counter } = setup();

    socket.emit('updates', [createUpdate(5)]);
    socket.emit('updates', [createUpdate(-10)]);

    expect(counter.getCount()).toBe(-5);
  });

  it('hydrates the counter', () => {
    const { socket, counter } = setup();

    socket.emit('hydrate', createState([2, 3, 10]));

    expect(counter.getCount()).toBe(15);
  });

  it('fires an event on updates', () => {
    const { socket, counter } = setup();
    const onUpdate = jest.fn();
    counter.subscribe(onUpdate);

    expect(onUpdate).not.toHaveBeenCalled();
    socket.emit('updates', [createUpdate(5)]);
    expect(onUpdate).toHaveBeenCalledWith(5);
  });

  it('fires on event on initial state', () => {
    const { socket, counter } = setup();
    const onUpdate = jest.fn();
    counter.subscribe(onUpdate);

    expect(onUpdate).not.toHaveBeenCalled();
    socket.emit('hydrate', createState([10, 15]));
    expect(onUpdate).toHaveBeenCalledWith(25);
  });

  it('returns an unsubscribe function', () => {
    const { socket, counter } = setup();
    const onUpdate = jest.fn();
    const unsubscribe = counter.subscribe(onUpdate);

    unsubscribe();
    socket.emit('updates', [createUpdate(10)]);

    expect(onUpdate).not.toHaveBeenCalled();
  });

  it('emits change events for add events', () => {
    const { counter } = setup();
    const onUpdate = jest.fn();
    counter.subscribe(onUpdate);

    counter.add(5);

    expect(onUpdate).toHaveBeenCalledWith(5);
  });

  it('broadcasts update events', () => {
    const { socket, counter } = setup();
    const onUpdate = jest.fn();

    socket.removeAllListeners();
    socket.on('updates', onUpdate);
    counter.add(25);

    expect(onUpdate).toHaveBeenCalledWith([
      expect.objectContaining({
        value: 25,
      }),
    ]);
  });

  it('emits the counter state on connection', () => {
    const { socket, counter } = setup();
    counter.add(5);
    counter.add(10);

    const onUpdate = jest.fn();
    socket.on('updates', onUpdate);
    socket.emit('connect');

    expect(onUpdate).toHaveBeenCalledWith([
      expect.objectContaining({ value: 5 }),
      expect.objectContaining({ value: 10 }),
    ]);
  });
});

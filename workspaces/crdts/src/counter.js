// @flow
import uuid from 'uuid/v4';

export const INVALID = 'nottodayhackers';

type Update = {
  operationId: string,
  value: number,
};

// Every operation is indexed by a uuid.
export const createUpdate = (value: number): Update => ({
  operationId: uuid(),
  value,
});

const isInvalidUpdate = (update: mixed): boolean => {
  if (!update) return true;
  if (typeof update.value !== 'number') return true;
  if (typeof update.operationId !== 'string') return true;
  if (!isFinite(update.value)) return true;

  return false;
};

export default function createCounter() {
  // Essentially a set of id/count tuples: Set<(id, count)>
  const operations: Map<string, number> = new Map();

  function merge(update: Update): Update {
    // Handle invalid updates by making them pointless. Without a guard,
    // invalid updates would propagate through other replicas, infecting them
    // with a crippling NaN virus.
    if (isInvalidUpdate(update)) {
      return { operationId: INVALID, value: 0 };
    }

    // Ideally this would immutably return a new value, but in this case it
    // would be wasteful. Automatically deduplicated since `.set` with the same
    // operation ID is idempotent.
    operations.set(update.operationId, update.value);

    return update;
  }

  // Derive the current state by summing all the values.
  const getCount = () => {
    const values = [...operations.values()];
    return values.reduce((sum, value) => sum + value, 0);
  };

  return {
    add: (count: number) => merge(createUpdate(count)),
    applyUpdate: merge,
    getCount,
    serialize() {
      const result: Update[] = [];

      for (const [operationId, value] of operations.entries()) {
        const update: Update = { operationId, value };
        result.push(update);
      }

      return result;
    },
  };
}

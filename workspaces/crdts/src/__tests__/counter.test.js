// @flow
import createCounter, { createUpdate, INVALID } from '../counter';

describe('createCounter(...)', () => {
  it('returns a counter', () => {
    const counter = createCounter();

    expect(counter).toMatchObject({
      applyUpdate: expect.any(Function),
      add: expect.any(Function),
      getCount: expect.any(Function),
    });
  });

  it('initializes the counter to 0', () => {
    const counter = createCounter();

    expect(counter.getCount()).toBe(0);
  });

  it('can increment the count', () => {
    const counter = createCounter();

    counter.add(5);
    counter.add(3);

    expect(counter.getCount()).toBe(8);
  });

  it('can increment by negative counts', () => {
    const counter = createCounter();

    counter.add(-5);
    counter.add(-15);

    expect(counter.getCount()).toBe(-20);
  });

  it('idempotently applies the given update', () => {
    const counter = createCounter();
    const update = createUpdate(5);

    counter.applyUpdate(update);
    counter.applyUpdate(update);
    counter.applyUpdate(update);

    expect(counter.getCount()).toBe(5);
  });

  it('mixes local updates with "upstream" updates', () => {
    const counter = createCounter();

    counter.add(1);
    counter.applyUpdate(createUpdate(2));
    counter.applyUpdate(createUpdate(4));
    counter.add(-8);

    expect(counter.getCount()).toBe(-1);
  });

  it('returns the generated update on add', () => {
    const counter = createCounter();
    const update = counter.add(5);

    expect(update).toEqual({
      operationId: expect.any(String),
      value: 5,
    });
  });

  it('ignores invalid values', () => {
    const counter = createCounter();

    expect(counter.add(('string': any))).toMatchObject({
      operationId: INVALID,
      value: 0,
    });

    expect(counter.add(-Infinity)).toMatchObject({
      operationId: INVALID,
      value: 0,
    });

    expect(counter.add(NaN)).toMatchObject({
      operationId: INVALID,
      value: 0,
    });

    expect(counter.getCount()).toBe(0);
  });

  it('ignores invalid keys', () => {
    const counter = createCounter();
    const update = { operationId: null, value: 10 };
    const operation = counter.applyUpdate((update: any));

    expect(operation.value).toBe(0);
    expect(operation.operationId).toBe(INVALID);

    expect(counter.getCount()).toBe(0);
  });

  it('survives if the update is falsy', () => {
    const counter = createCounter();
    const update = counter.applyUpdate((null: any));

    expect(update.value).toBe(0);
    expect(update.operationId).toBe(INVALID);
  });

  describe('createUpdate(...)', () => {
    it('returns an update', () => {
      const update = createUpdate(30);

      expect(update).toMatchObject({
        operationId: expect.any(String),
        value: 30,
      });
    });
  });

  describe('serialize(...)', () => {
    it('returns a JSON-compatible value', () => {
      const counter = createCounter();

      counter.add(5);
      counter.add(15);
      const operations = counter.serialize();

      expect(operations).toHaveLength(2);
      expect(operations).toEqual([
        expect.objectContaining({ value: 5 }),
        expect.objectContaining({ value: 15 }),
      ]);
    });
  });
});

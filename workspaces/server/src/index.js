// @flow
import { createServer } from 'http';
import express from 'express';
import io from 'socket.io';
import path from 'path';

import relay from './relay';

const app = express();
const server = createServer(app);
relay(io().attach(server));

const APPS = {
  COUNTER: path.join(__dirname, '../../counter/build'),
};

app.use('/counter', express.static(APPS.COUNTER));

app.get('*', (req, res) => {
  res
    .status(404)
    .end('I was too lazy to make a proper 404 page. Deal with it.');
});

server.listen(process.env.PORT || 8080);

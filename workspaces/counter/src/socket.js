// @flow
import openSocket from 'socket.io-client';

export default openSocket('/counter');

// @flow
import { createCounter } from '@crdt-demo/crdts';
import { shallow } from 'enzyme';
import React from 'react';

import { CountProvider } from '../count-provider';

describe('Count provider', () => {
  const setup = initialCount => {
    const Component = () => null;
    const unsubscribe = jest.fn();
    const counter = {
      ...createCounter(),
      subscribe: jest.fn(() => unsubscribe),
    };

    if (typeof initialCount === 'number') {
      counter.add(initialCount);
    }

    const props = {
      children: <Component />,
      counter,
    };

    const output = shallow(<CountProvider {...props} />);

    return {
      unsubscribe,
      Component,
      output,
      props,
      emitChange(value) {
        expect(counter.subscribe).toHaveBeenCalledWith(expect.any(Function));

        const [fn] = counter.subscribe.mock.calls[0];
        fn(value);

        output.update();
      },
    };
  };

  it('renders', () => {
    expect(setup).not.toThrow();
  });

  it('renders the children', () => {
    const { output, Component } = setup();

    expect(output.find(Component).exists()).toBe(true);
  });

  it('adds the count to children', () => {
    const { output } = setup();

    expect(output.prop('count')).toBe(0);
  });

  it('initializes with the correct count', () => {
    const { output } = setup(10);

    expect(output.prop('count')).toBe(10);
  });

  it('updates the count on change', () => {
    const { output, emitChange } = setup();

    emitChange(30);

    expect(output.prop('count')).toBe(30);
  });

  it('unsubscribes from the count on unmount', () => {
    const { output, unsubscribe } = setup();
    output.unmount();

    expect(unsubscribe).toHaveBeenCalled();
  });

  it('passes through the add(...) handler', () => {
    const { output, props } = setup();

    expect(output.prop('add')).toBe(props.counter.add);
  });
});

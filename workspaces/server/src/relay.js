// @flow
import { createCounter } from '@crdt-demo/crdts';

export const EVENTS = {
  HYDRATE: 'hydrate',
  UPDATES: 'updates',
};

export const ROOMS = {
  COUNTER: '/counter',
};

export default function relay(io: *) {
  const counter = createCounter();

  io.of(ROOMS.COUNTER).on('connect', socket => {
    socket.emit(EVENTS.HYDRATE, counter.serialize());

    socket.on(EVENTS.UPDATES, updates => {
      Array.prototype.forEach.call(updates || [], counter.applyUpdate);
      socket.broadcast.emit(EVENTS.UPDATES, updates);
    });
  });
}

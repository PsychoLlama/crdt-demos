import { shallow } from 'enzyme';
import React from 'react';

import { App, Count, Decrement, Increment } from '../App';

describe('App', () => {
  const setup = merge => {
    const props = {
      add: jest.fn(),
      count: 30,
      ...merge,
    };

    return {
      output: shallow(<App {...props} />),
      props,
    };
  };

  it('renders', () => {
    expect(setup).not.toThrow();
  });

  it('shows the count', () => {
    const { output, props } = setup();
    const count = output.find(Count);

    expect(count.prop('children')).toBe(props.count);
  });

  it('increments when the button is clicked', () => {
    const { output, props } = setup();
    output.find(Increment).simulate('click');

    expect(props.add).toHaveBeenCalledWith(1);
  });

  it('decrements when the button is clicked', () => {
    const { output, props } = setup();
    output.find(Decrement).simulate('click');

    expect(props.add).toHaveBeenCalledWith(-1);
  });
});

// @flow
import React, { type Element as ReactElement } from 'react';

type Props<Child> = {
  children: Child,
  counter: {
    subscribe((number) => mixed): () => void,
    getCount(): number,
    add(number): void,
  },
};

type State = {
  count: number,
};

export class CountProvider<Child: ReactElement<*>> extends React.Component<
  Props<Child>,
  State
> {
  state = { count: this.props.counter.getCount() };
  unsubscribe: () => void;

  componentDidMount() {
    this.unsubscribe = this.props.counter.subscribe(this.updateCount);
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  updateCount = (count: number) => {
    this.setState({ count });
  };

  render() {
    const child = React.Children.only(this.props.children);

    return React.cloneElement(child, {
      add: this.props.counter.add,
      count: this.state.count,
    });
  }
}

export default CountProvider;

// @flow
import { createCounter } from '@crdt-demo/crdts';
import Emitter from 'events';

import relay, { EVENTS, ROOMS } from '../relay';

describe('Socket relay', () => {
  class Socket extends Emitter {
    broadcast: Emitter;

    constructor() {
      super();
      this.broadcast = new Emitter();
    }
  }

  class Server extends Emitter {
    rooms: Map<string, Server>;

    constructor() {
      super();
      this.rooms = new Map();
    }

    of(roomName) {
      const room = new Server();
      this.rooms.set(roomName, room);

      return room;
    }

    expectRoom(roomName) {
      const room = this.rooms.get(roomName);

      if (!room) {
        throw new Error(`Server isn't using the ${roomName} room.`);
      }

      return room;
    }
  }

  const createUpdate = () => {
    const counter = createCounter();
    return counter.add(5);
  };

  const setup = () => {
    const server = new Server();
    relay(server);

    return {
      server,
      addSocket: (room: Server, socket = new Socket()) => {
        room.emit('connection', socket);
        room.emit('connect', socket);

        return socket;
      },
    };
  };

  it('relays updates to other sockets', () => {
    const { server, addSocket } = setup();
    const update = createUpdate();
    const onUpdate = jest.fn();
    const socket = addSocket(server.expectRoom(ROOMS.COUNTER));

    socket.broadcast.on(EVENTS.UPDATES, onUpdate);
    socket.emit(EVENTS.UPDATES, [update]);

    expect(onUpdate).toHaveBeenCalledWith([update]);
  });

  it('updates the local counter replica', () => {
    const { server, addSocket } = setup();
    const update = createUpdate();
    const room = server.expectRoom(ROOMS.COUNTER);
    addSocket(room).emit(EVENTS.UPDATES, [update]);
    const socket = new Socket();

    const onHydrate = jest.fn();
    socket.on(EVENTS.HYDRATE, onHydrate);
    addSocket(room, socket);

    expect(onHydrate).toHaveBeenCalledWith([
      expect.objectContaining({ value: update.value }),
    ]);
  });

  // Ha, like that would ever happen.
  it('survives when given invalid updates', () => {
    const { server, addSocket } = setup();
    const socket = addSocket(server.expectRoom(ROOMS.COUNTER));
    const pass = () => socket.emit(EVENTS.UPDATES, null);

    expect(pass).not.toThrow();
  });
});

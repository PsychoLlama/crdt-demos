// @flow
import { FiChevronUp, FiChevronDown } from 'react-icons/fi';
import React, { type ComponentType } from 'react';
import styled from 'styled-components';

const Container = styled.main`
  display: flex;
  flex-grow: 1;
  justify-content: center;
  flex-direction: column-reverse;
  align-items: center;
  color: #aaa;
`;

export const Count = styled.div`
  font-size: 24px;
`;

const Button = styled.button`
  border: none;
  outline: none;
  background-color: transparent;
  font-size: 45px;
  padding: 0;
  color: #aaa;
  transition: color 250ms ease-in-out;

  :hover {
    color: #888;
  }

  :active {
    color: #777;
  }
`;

export const Increment: ComponentType<Object> = styled(Button)``;
export const Decrement: ComponentType<Object> = styled(Button)``;

type Props = {
  add(number): void,
  count?: number,
};

export class App extends React.Component<Props> {
  render() {
    return (
      <Container>
        <Decrement onClick={this.decrement}>
          <FiChevronDown />
        </Decrement>

        <Count>{this.props.count}</Count>

        <Increment onClick={this.increment}>
          <FiChevronUp />
        </Increment>
      </Container>
    );
  }

  increment = () => {
    this.props.add(1);
  };

  decrement = () => {
    this.props.add(-1);
  };
}

export default App;

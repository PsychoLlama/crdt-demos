// @flow
// $FlowFixMe
import styled, { createGlobalStyle } from 'styled-components';
import ReactDOM from 'react-dom';
import React from 'react';

import synchronizeCounter from './utils/synchronize-counter';
import ProvideCount from './utils/count-provider';
import socket from './socket';
import App from './App';

const Style = createGlobalStyle`
  body {
    margin: 0;
    min-height: 100vh;
    background-color: #f0f0f0;
    display: flex;
    font-family: Helvetica, sans-serif;
  }

  #root {
    display: flex;
    flex-grow: 1;
  }
`;

const Container = styled.div`
  display: flex;
  flex-grow: 1;
`;

const root: HTMLDivElement = (document.getElementById('root'): any);
ReactDOM.render(
  <Container>
    <Style />

    <ProvideCount counter={synchronizeCounter(socket)}>
      <App />
    </ProvideCount>
  </Container>,
  root
);
